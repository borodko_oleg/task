import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 */
public class Fibo {

    public static void main(String[] args) throws Exception {

        System.out.println("Сколько чисел Фибоначчи нужно вывести? (положит. число)");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String snum = reader.readLine();

        int num = Integer.parseInt(snum);

        int buffer1=0;
        int buffer2=1;
        int sum = 0;

        for (int i=0;i<num;i++) {

            if (i==0) {
                System.out.print("0 ");
            }
            else if (i==1) {
                System.out.print("1 ");
            }
            else {
                sum=buffer1+buffer2;
                System.out.print(sum+" ");
                buffer1=buffer2;
                buffer2=sum;
            }
        }
    }
}
