import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * Created 17.10.2014.
 */
public class Faktorial {
    public static void main(String[] args) throws Exception {


        System.out.println("Введите положительное число");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String snum = reader.readLine();

        int num = Integer.parseInt(snum);
        int a = num;

        if (a == 0) {
            System.out.println("Результат 1");
        }

        if (a < 0) {
            System.out.println("Вы ввели отрицательое число");
        }

        if (a > 0) {
            for (int i = 1; i < a; i++) {
                num = num * i;
            }

            System.out.println(num);
        }
    }
}