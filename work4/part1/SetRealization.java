import java.util.HashSet;
import java.util.Set;

/**
 * Created by oleg on 01.12.2014.
 */
public class SetRealization implements SetOperations {

    public static void main(String[] args) {

        SetRealization my_realization = new SetRealization();

        Set a=new HashSet();
        Set b=new HashSet();
        

        a.add("apple");
        a.add("banana");
        a.add("orange");
        a.add("lemon");

        b.add("apple");
        b.add("banana");
        b.add("iris");
        b.add("barbaris");
        b.add("lemon");


        System.out.println(my_realization.equals(a, b));
        System.out.println(my_realization.union(a, b));

        System.out.println(my_realization.subtract(a, b));
        System.out.println(my_realization.intersect(a, b));

        System.out.println(my_realization.symmetricSubtract(a,b));

    }

    public void out_set(Set a){

        for (Object fruit :  a) {
            System.out.println(fruit);
        }
    }

    @Override
    public boolean equals(Set a, Set b) {
        if(a.equals(b)) return true;
        return false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set c=new HashSet();
        c.addAll(a);
        c.addAll(b);
        if (c.size()>0) return c;

        return null;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set c=new HashSet();
        c.addAll(a);
        c.removeAll(b);
        if (c.size()>0) return c;

        return null;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set c;
        //c.addAll(a);
        //c.addAll(b);
        c=union(a,b);
        c.retainAll(a);
        c.retainAll(b);

        if (c.size()>0) return c;
        return null;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set c;
        Set d;
        //c.addAll(a);
        //c.addAll(b);
        //c.retainAll(a);
        //c.retainAll(b);
        c=intersect(a,b);
        //d.addAll(a);
        //d.addAll(b);
        d=union(a,b);
        d.removeAll(c);
        if (d.size()>0) return d;

        return null;
    }
}
