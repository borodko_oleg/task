import java.util.Arrays;

/**
 * Created by oleg on 30.11.2014.
 */
public class ComparableTest {

    public static void main(String[] args) {


        Dog more_dogs[] = new Dog[3];
        more_dogs[0] = new Dog(1.6,"Sharik");
        more_dogs[1] = new Dog(2,"Kubik");
        more_dogs[2] = new Dog(1,"Leniviz");

        Dog more_dogs_copy[]=new Dog[3];

        System.arraycopy(more_dogs,0,more_dogs_copy,0,3);


        Arrays.sort(more_dogs_copy);

        for(Dog e : more_dogs_copy){
           e.description();
        }

        System.out.println("------------");

        for(Dog e : more_dogs){
            e.description();
        }

    }


}

class Dog implements Comparable<Dog>{

    private double age;
    private String name;

    public Dog(double age, String name){
        this.age=age;
        this.name=name;
    }

    public void description(){
        System.out.println(name + ' '+ age);
    }

    @Override
    public int compareTo(Dog e) {
        if (age < e.age) return -1;
        if (age > e.age) return 1;
        return 0;
    }

}
