import java.io.IOException;

/**
 * Created by oleg on 10.11.2014.
 */
public class MyCar extends Vehicle{

    private int quantity_fuel=10; //кол-во топлива в литрах в баке
    private int volume_tank=400; // объем бака в литрах
    private int max_speed=130; // макс. скорость км в ч
    private int intfuel_consumption_on_100km=8;  // расход топлива на 100 км литров
    private int speed=0;  // скорость в километрах текущая
    private int technical_status=50; //техническое состояние в процентах
    private boolean motor_status=false; //заведен или нет

    private void motor(){
        //будет пожирать топливо , заводится если есть бензин
        if (technical_status>30 && quantity_fuel>0 && motor_status==false && speed==0) {
            System.out.println("мотор заработал...");
            motor_status=true;
        }else {
            System.out.println("мотор не заводится или уже работает..");
        }

    }

    public void drive_100km() throws IOException {
        //уменьшает бензин и состояние машины
        if  (motor_status==true) {
            if (technical_status<30){
                technical_status=0;
                throw new IOException("машина сламалась в пути..");
            }else
            {

                if (quantity_fuel<intfuel_consumption_on_100km) {
                    quantity_fuel=0;
                    //машина сламалась

                    System.out.println("вы проехали только "+quantity_fuel*10 +"км");

                    throw new IOException("бензин закончился");

                }else{
                    quantity_fuel-=intfuel_consumption_on_100km;
                    technical_status-=10;
                    System.out.println("вы успешно проехали 100км");
                }

            }

        }else System.out.println("заведи мотор и ехай");
    }

    public void refuel_10(){  //заправиться
        if (speed==0 && quantity_fuel<=volume_tank-10){
            quantity_fuel+=10;
            System.out.println(" 10л топлива заправлено.. ");
        }else try {
            throw new IOException("переполнение бака");
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    @Override
    public void accelerate() {
        // пытается запустить мотор и поехать
            motor();
    }

    @Override
    public void brake() {
        //останавливается
        //сетуация когда машина завелась в норм состоянии и сламалась по дороге, остановится не выйдет
        if (technical_status<30 && motor_status==true) {
            System.out.println("тормаза отказали .. Авария!! .. ");
            technical_status=0;
            quantity_fuel=0;
        }else {
            System.out.println("машина стала..");
            motor_status=false;
            speed=0;
        }
    }

    @Override
    public void turn_left() {
        //поворачивает влево
        if (speed==0) {
            System.out.println(" что-то пошло не так.. ");
        }else System.out.println(" машина повернула влево.. ");
    }

    @Override
    public void turn_right() {
        if (speed==0) {
            System.out.println(" что-то пошло не так.. ");
        }else System.out.println(" машина повернула вправо.. ");
    }

    @Override
    public void speed_up() {
        if (motor_status==false || speed==max_speed) {
            System.out.println(" что-то пошло не так.. ");
        }else {
            System.out.println("машина ускорилась.. ");
            speed+=10;
            quantity_fuel-=1;
        }
    }

    @Override
    public void speed_down() {
        if (speed==0) {
            System.out.println(" что-то пошло не так.. ");
        }else {
            System.out.println("машина замедлилась.. ");
            speed-=10;
        }
    }

    @Override
    public void info() {
        //будет показывать бензин , техническое состояние
        if (motor_status==true) {
            System.out.println("бензин=" + quantity_fuel);
            System.out.println("скорость=" + speed);
        }else
            System.out.println("техническое состояние="+technical_status);

    }

    @Override
    public void repair() {
        //поднимает тех состояние

        technical_status= repair_mechanism(speed,technical_status,"починили машину","что-то пошло не так");
    }


}
