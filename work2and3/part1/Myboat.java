/**
 * Created by oleg on 11.11.2014.
 */
public class Myboat extends Vehicle {



    private void motor(){
            System.out.println("мотор лодки заработал...");
    }

    public void refuel(){  //заправиться
       System.out.println("заправили бак.. ");
    }

    @Override
    public void accelerate() {
        // пытается запустить мотор и поехать
        motor();
    }

    @Override
    public void brake() {
            System.out.println("выключили мотор, плывем по олнам");
    }

    @Override
    public void turn_left() {
        //поворачивает влево
        System.out.println(" крутое пике влево.. ");
    }

    @Override
    public void turn_right() {
        System.out.println(" крутое пике вправо.. ");
    }

    @Override
    public void speed_up() {
            System.out.println("пошли ускоряться против ветра");
    }

    @Override
    public void speed_down() {
            System.out.println("тише ато лодка перевернется ");
    }

    @Override
    public void info() {
        //будет показывать бензин , техническое состояние
        System.out.println("дно в порядке, не течем");
    }

    @Override
    public void repair() {
        //поднимает тех состояние

        System.out.println("починили наше корыто");
    }
}
