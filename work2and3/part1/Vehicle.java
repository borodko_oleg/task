/**
 * Created by oleg on 10.11.2014.
 */
public abstract class Vehicle implements Driveable{


    public int repair_mechanism(int speed, int technical_status, String ok, String error) {
        //поднимает тех состояние
        if (speed==0 && technical_status<100) {
            technical_status += 7;
            System.out.println(ok);

        }else {
            System.out.println(error);
        }
        return technical_status;
    }


    abstract public void info(); //
    abstract public void repair(); //

    abstract public void turn_left();
    abstract public void turn_right();

    abstract public void speed_up(); //повысить скорость
    abstract public void speed_down(); //понизить скорость



}
