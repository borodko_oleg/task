import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

/**
 * Created by oleg on 10.11.2014.
 */
public class Start {

    public static void main(String[] args) throws IOException {

        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Выбирите транспортное средство");
        System.out.print("1- машина на бензине ");
        System.out.print("2- лодка ");
        System.out.println("3- машина на солнечн. батареях");


        String vehicle=reader.readLine();

        //


        Object object;

        MyCar object1=new MyCar();
        Myboat object2=new Myboat();
        SolarPoweredCar object3=new SolarPoweredCar();

        if (vehicle.equals("1")) {
            object=object1;

        } else if (vehicle.equals("2")) {
            object=object2;

        } else  {
            object=object3;
        }


        Podmenu(object);

        while (true) {

            String command = reader.readLine();

            Menu((Vehicle) object, command);

        }


    }


    public static void Menu(Vehicle object, String comand) throws IOException {

        if (comand.equals("1")) {
            object.accelerate();

        } else if (comand.equals("2")) {
            object.brake();

        } else if (comand.equals("3")) {
            object.info();

        } else if (comand.equals("4")) {
            object.repair();

        } else if (comand.equals("5")) {
            object.turn_left();

        } else if (comand.equals("6")) {
            object.turn_right();

        } else if (comand.equals("7")) {
            object.speed_up();

        } else if (comand.equals("8")) {
            object.speed_down();

        }else if (comand.equals("10") && object instanceof MyCar) {
            ((MyCar) object).refuel_10();

        }else if (comand.equals("100") && object instanceof MyCar) {
            ((MyCar) object).drive_100km();

        } else if (comand.equals("9")) {

            Podmenu(object);

        }else {
            System.exit(1);
        }


    }

    public static void Podmenu(Object o)
    {
        System.out.println("==============================");
        System.out.println("9- доступные операции");
        System.out.println("1- accelerate");
        System.out.println("2- brake");
        System.out.println("3- info");
        System.out.println("4- repair");
        System.out.println("5- turn_left");
        System.out.println("6- turn_right");
        System.out.println("7- speed_up");
        System.out.println("8- speed_down");
        System.out.println("0- exit");
        if (o instanceof MyCar){
            System.out.println("10- refuel 10 l");
            System.out.println("100- drive_100km");
        }
    }
}
