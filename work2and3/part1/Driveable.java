/**
 * Created by oleg on 10.11.2014.
 */
public interface Driveable {
    public void accelerate();
    public void brake();
}
